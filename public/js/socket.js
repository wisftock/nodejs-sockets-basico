const socket = io();
socket.on('connect', function () {
  console.log('conectado al servidor');
});

// desconexion del usuario o servidor
socket.on('disconnect', function () {
  console.log('perdimos conexion con el servidor');
});

// enviar informacion
socket.emit(
  'enviarMensaje',
  {
    user: 'pepe',
    message: 'Hola mundo',
  },
  function (respt) {
    console.log('respuesta server: ', respt);
  }
);

// escuchar informacion
socket.on('enviarMensaje', function (message) {
  console.log('Servidor :', message);
});
