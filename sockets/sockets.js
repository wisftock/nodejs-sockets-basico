const { io } = require('../app');

io.on('connection', (client) => {
  console.log('usuario conectado');

  client.emit('enviarMensaje', {
    user: 'Administrador',
    message: 'Bienvenidos a sockets',
  });

  // desconexion del usuario o servidor
  client.on('disconnect', () => {
    console.log('usuario desconectado');
  });
  // escuchar el cliente
  client.on('enviarMensaje', (data, callback) => {
    console.log(data);

    client.broadcast.emit('enviarMensaje', data);

    // callback();
    // if (message.user) {
    //   callback({
    //     respt: 'TODO SALIO BIEN!!!',
    //   });
    // } else {
    //   callback({
    //     respt: 'TODO SALIO MAL!!!!!!',
    //   });
    // }
  });
});
